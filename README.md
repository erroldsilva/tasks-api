# Task API

The Task API is a RESTful API for managing tasks.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js and npm installed on your local machine.
- Docker installed on your local machine.

## Getting Started

To get started with this project, follow these steps:

1. Clone this repository to your local machine:

   ```bash
   git clone ....
   ```

2. Navigate to the project directory:

   ```bash
   cd tasks-api
   ```

3. Install the project dependencies:

   ```bash
   npm install
   ```

4. The default configuration can be found in `config\config.json` if the docker-compose is used it should work out of
   the box.

5. Start PostgreSQL database in a Docker container:

   ```bash
   docker compose up -d
   ```

6. Apply database migrations to create the necessary tables:

   ```bash
   npm run migrate
   ```

7. Seed the database with initial data (optional):

   ```bash
   npm run seed
   ```

8. Start the API server:

   ```bash
   npm start
   ```

   Server will be accessible at `http://localhost:9000`.

## API Documentation

```
http://localhost:9000/api/docs
```

## Project Structure
* `src/controller`: this has the main application controller where the routes are defined & the end-to-end tests for the controllers
* `src/services/*.js`: this contains the business logic the service files act as the interaction layer with other modules
* `src/services/usecases`: this where the business logic resides, these methods have no external dependencies they all required information as parameters and work on them, this keeps the business logic isolated and makes it easy to verify with simple unit test
* `src/repositiry`: this is the interaction layer with the db

## Testing

To run tests for the API, use the following command:

Unit tests:

```bash
npm test:unit
```

API tests:

```bash
npm test:api
```

## Design Considerations

* The project is created to demonstrate my approach to writing API's with Node
* Use of fastify, sequelize is to experiment with these
* Use of JS over TS is again to show my comfort level with JS
* I wanted to time box this, so I do not end up spending way more time on this and hence I did the minimum needed to
  demonstrate my approach to API building
* From a design perspective to save on time I have just 1 entity model which saves both the task & the assignment, in
  ideal situation for production I would have split them to 2 different entities with 1 (task)-> N (assignment)
  relationship
* The APIs also are optimised to create an MVP, certain additional API's required would be to fetch the list of
  assignments, get assignment for a certain user
* The `usecases` is an experiment I am doing to try and add the best part of "clean architecture" without the other over
  head, as IMHO the business needs change, but application fundamentals like data access etc does not, but after
  implementing it I am now wondering how to deal with validations etc with changing business, maybe another layer is
  needed, so its a basic experiment
* Additionally I would maybe add DI container so the modules can be injected dynamically at run time, but without
  interfaces and this (and annotations) is the only reason I would use TS over JS, if not I am a JS fanboy ;)  
