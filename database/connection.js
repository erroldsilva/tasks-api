import fs from 'fs';

import path from 'path';

import { Sequelize } from 'sequelize';

const environment = process.env.NODE_ENV || 'development';

// Read the database configuration from config.json
const configPath = path.join(path.resolve(), 'config', 'config.json');
const configJson = fs.readFileSync(configPath, 'utf-8').toString();
const config = JSON.parse(configJson)[environment];

// Create a Sequelize instance using the configuration from config.json
const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  dialect: config.dialect,
  logging: process.env.DB_LOGGING || false,
  // Add other Sequelize options as needed
});

export default sequelize;
