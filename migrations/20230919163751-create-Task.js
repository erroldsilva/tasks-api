'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Tasks', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      taskId: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
      },
      location: {
        type: Sequelize.ARRAY(Sequelize.DECIMAL),
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
      },
      status: {
        type: Sequelize.ENUM('pending', 'completed'),
        defaultValue: 'pending',
      },
      assignedTo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      assignedAt: {
        type: Sequelize.UUID,
        allowNull: true,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
    await queryInterface.addIndex({
      name: 'index_taskId',
      unique: true,
      fields: ['taskId'],
    }, {
      name: 'index_location',
      unique: true,
      fields: ['location'],
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Tasks', ['index_taskId', 'index_location']);
    await queryInterface.dropTable('Tasks');
  },
};
