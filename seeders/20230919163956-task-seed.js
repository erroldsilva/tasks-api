const crypto = require('crypto');

const acceptedStatus = ['pending', 'in-progress', 'completed'];
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Seed data for the Task model with random values
    const tasks = [];

    for (let i = 1; i <= 10; i++) {
      const randomLocation = [i + 10, i + 20];
      const randomDescription = `Task ${i} description`;
      const randomStatus = acceptedStatus[i % 3];
      const randomAssignedTo = `User ${Math.floor(Math.random() * 5) + 1}`;
      const randomAssignedAt = new Date(
        Date.now() - Math.floor(Math.random() * 86400000),
      );
      const createdAt = new Date();
      const updatedAt = new Date();

      tasks.push({
        taskId: crypto.randomUUID(),
        location: randomLocation,
        description: randomDescription,
        status: randomStatus,
        assignedTo: randomAssignedTo,
        assignedAt: randomAssignedAt,
        createdAt: createdAt,
        updatedAt: updatedAt,
      });
    }

    await queryInterface.bulkInsert('Tasks', tasks, {});
  },

  down: async (queryInterface, Sequelize) => {
    // Remove all seed data
    await queryInterface.bulkDelete('Tasks', null, {});
  },
};
