import fastify from 'fastify';
import taskRoutes from './router.js';
import fastifySwagger from '@fastify/swagger';
import fastifySwaggerUi from '@fastify/swagger-ui';


const server = fastify();
(async () => {
  await server.register(fastifySwagger);
  await server.register(fastifySwaggerUi, {
    routePrefix: '/api/docs',
    swagger: {
      info: {
        title: 'Task Management API',
        description: 'API documentation for the Task Management API',
        version: '1.0.0',
      },
      host: 'localhost:9000',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
    exposeRoute: true,
  });
  await server.register(taskRoutes, { prefix: '/v1' });
  await server.ready();
  server.swagger();
})();

export default server;
