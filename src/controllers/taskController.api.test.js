import fastify from '../app.js';
import supertest from 'supertest';


describe('API Tests', () => {
  let request;

  beforeAll(async () => {
    await fastify.ready();
    request = supertest(fastify.server);
  });

  afterAll(async () => {
    await fastify.close();
  });

  it('should create a new task', async () => {
    const response = await request
      .post('/v1/api/tasks')
      .send({
        location: [0, 1],
        description: 'Test Description',
      });
    expect(response.statusCode).toBe(201);
    expect(response.body.status).toBe('pending');
  });

  it('should retrieve a task by ID', async () => {
    const newTask = await request
      .post('/v1/api/tasks')
      .send({
        location: [0, 1],
        description: 'Test Description',
      });
    const taskId = newTask.body.taskId;
    const response = await request.get(`/v1/api/tasks/${taskId}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.status).toBe('pending');
    expect(response.body.taskId).toBe(taskId);
  });

  it('should assign a task by to an assignee', async () => {
    const assignedTo = '3fa85f64-5717-4562-b3fc-2c963f66afa6';
    const newTask = await request
      .post('/v1/api/tasks')
      .send({
        location: [0, 1],
        description: 'Test Description',
      });
    const taskId = newTask.body.taskId;
    const response = await request.put(`/v1/api/tasks/${taskId}/assign`)
      .send({
        assignedTo,
      });

    expect(response.statusCode).toBe(200);
    expect(response.body.taskId).toBe(taskId);
    expect(response.body.assignedTo).toBe(assignedTo);
  });

  it('should check pagination works', async () => {
    await Promise.all([
      request
        .post('/v1/api/tasks')
        .send({
          location: [0, 1],
          description: 'Test Description',
        }),
      request
        .post('/v1/api/tasks')
        .send({
          location: [0, 1],
          description: 'Test Description',
        }),
    ]);
    const response = await Promise.all([
      request.get(`/v1/api/tasks?page=1&pageSize=1`),
      request.get(`/v1/api/tasks?page=2&pageSize=1`),
    ]);
    expect(response.length).toBe(2);
    expect(response[0].body.tasks.length).toBe(1);
    expect(response[0].body.totalTasks).toBeGreaterThan(1);
    expect(response[0].body.totalPages).toBeGreaterThan(1);
    expect(response[1].body.tasks.length).toBe(1);
  });

});
