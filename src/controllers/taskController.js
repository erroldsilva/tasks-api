/**
 * @swagger
 * tags:
 *   name: Tasks
 *   description: API operations related to tasks
 */

import { addAssignee, createTask, getTaskById, listTasks } from '../services/taskService.js';

/**
 * @swagger
 * /api/tasks:
 *   post:
 *     summary: Create a new task.
 *     tags: [Tasks]
 *     requestBody:
 *       required: true
 *       description: Task data to create.
 *       content:
 *         application/json:
 *           schema: ${taskSchemas.createTaskSchema}
 *     responses:
 *       '201':
 *         description: Task created successfully.
 *         content:
 *           application/json:
 *             schema: ${taskSchemas.taskSchema}
 *       '400':
 *         description: Invalid request. Check the request body.
 *       '500':
 *         description: Internal server error.
 */
export async function createTaskHandler(request, reply) {
  try {
    const task = await createTask(request.body);
    reply.code(201).send(task);
  } catch (error) {
    console.log(error);
    reply.code(500).send({ error: error.message });
  }
}

/**
 * @swagger
 * /api/tasks/{taskId}:
 *   get:
 *     summary: Get a task by ID.
 *     tags: [Tasks]
 *     parameters:
 *       - in: path
 *         name: taskId
 *         required: true
 *         description: ID of the task to retrieve.
 *         schema:
 *           type: string
 *           format: uuid
 *     responses:
 *       '200':
 *         description: Task retrieved successfully.
 *         content:
 *           application/json:
 *             schema: ${taskSchemas.taskSchema}
 *       '404':
 *         description: Task not found. Check the taskId parameter.
 *       '500':
 *         description: Internal server error.
 */
export async function getTaskByIdHandler(request, reply) {
  const taskId = request.params.taskId;
  try {
    const task = await getTaskById(taskId);
    reply.code(200).send(task);
  } catch (error) {
    reply.code(500).send({ error: error.message });
  }
}

/**
 * @swagger
 * /api/tasks:
 *   get:
 *     summary: List all tasks.
 *     tags: [Tasks]
 *     responses:
 *       '200':
 *         description: List of tasks retrieved successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 tasks:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Task'
 *                 totalTasks:
 *                   type: integer
 *                   description: Total number of tasks.
 *                 currentPage:
 *                   type: integer
 *                   description: Current page number.
 *                 totalPages:
 *                   type: integer
 *                   description: Total number of pages.
 *       '500':
 *         description: Internal server error.
 */
export async function listTasksHandler(request, reply) {
  try {
    const tasks = await listTasks(request.query.page, request.query.pageSize);
    reply.code(200).send(tasks);
  } catch (error) {
    reply.code(500).send({ error: error.message });
  }
}


/**
 * @swagger
 * /api/tasks/{taskId}/assign:
 *   put:
 *     summary: Assign a task to a worker.
 *     tags: [Tasks]
 *     parameters:
 *       - in: path
 *         name: taskId
 *         required: true
 *         description: ID of the task to assign.
 *         schema:
 *           type: string
 *           format: uuid
 *     requestBody:
 *       required: true
 *       description: Worker ID or name to assign to the task.
 *       content:
 *         application/json:
 *           schema: ${taskSchemas.assignTaskSchema}
 *     responses:
 *       '200':
 *         description: Task assigned successfully.
 *         content:
 *           application/json:
 *             schema: ${taskSchemas.taskSchema}
 *       '400':
 *         description: Invalid request. Check the request body or taskId parameter.
 *       '500':
 *         description: Internal server error.
 */
export async function assignTaskHandler(request, reply) {
  const taskId = request.params.taskId;
  const { assignedTo } = request.body;
  try {
    const assignedTask = await addAssignee(taskId, assignedTo);
    reply.code(200).send(assignedTask);
  } catch (error) {
    console.log(error);
    reply.code(500).send({ error: error.message });
  }
}


