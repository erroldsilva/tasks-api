export const createTaskSchema = {
  body: {
    type: 'object',
    properties: {
      location: {
        type: 'array',
        items: {
          type: 'number',
        },
        minItems: 2,
        maxItems: 2,
      },
      description: { type: 'string' },
    },
    required: ['location', 'description'],
  },
  response: {
    default: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          default: true,
        },
      },
    },
    '2xx': {
      type: 'object',
      properties: {
        taskId: { type: 'string' },
        status: { type: 'string', enum: ['pending'] },
        createdAt: { type: 'string', format: 'date-time' },
      },
    },
  },
};

// Define a schema for assigning a task to a worker
export const assignTaskSchema = {
  params: {
    type: 'object',
    properties: {
      taskId: { type: 'string', format: 'uuid' },
    },
  },
  body: {
    type: 'object',
    properties: {
      assignedTo: { type: 'string', format: 'uuid' },
    },
    required: ['assignedTo'],
  },
  response: {
    default: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          default: true,
        },
      },
    },
    '2xx': {
      type: 'object',
      properties: {
        taskId: { type: 'string' },
        status: { type: 'string', enum: ['pending'] },
        assignedTo: { type: 'string' },
        assignedAt: { type: 'string', format: 'date-time' },
      },
    },
  },
};

export const getTaskByIdSchema = {
  params: {
    type: 'object',
    properties: {
      taskId: { type: 'string', format: 'uuid' },
    },
  },
  response: {
    default: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          default: true,
        },
      },
    },
    '2xx': {
      type: 'object',
      properties: {
        taskId: { type: 'string' },
        title: { type: 'string' },
        status: { type: 'string', enum: ['pending', 'in-progress', 'completed'] },
        assignedTo: { type: 'string' },
        createdAt: { type: 'string', format: 'date-time' },
        location: {
          type: 'array',
          items: { type: 'number' },
          minItems: 2,
          maxItems: 2,
        },
      },
    },
  },
};

export const getAllTasksSchema = {
  querystring: {
    type: 'object',
    properties: {
      page: { type: 'integer', minimum: 1 }, // Validate that page is a positive integer
      pageSize: { type: 'integer', minimum: 1, maximum: 100 }, // Validate pageSize within a reasonable range
    },
    required: [], // You can specify required query parameters if needed
  },
  response: {
    default: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          default: true,
        },
      },
    },
    '2xx': {
      type: 'object',
      properties: {
        tasks: {
          type: 'array',
          items: {
            properties: {
              taskId: { type: 'string' },
              title: { type: 'string' },
              status: { type: 'string', enum: ['pending', 'in-progress', 'completed'] },
              assignedTo: { type: 'string' },
              createdAt: { type: 'string', format: 'date-time' },
              location: {
                type: 'array',
                items: { type: 'number' },
                minItems: 2,
                maxItems: 2,
              },
            },
          },
        },
        totalTasks: { type: 'integer' },
        currentPage: { type: 'integer' },
        totalPages: { type: 'integer' },
      },
    },
  },
};


