// models/Task.js
import { DataTypes } from 'sequelize';

import sequelize from '../../database/connection.js'; // Import your Sequelize connection
const Task = sequelize.define('Task', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  taskId: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
  },
  location: {
    type: DataTypes.ARRAY(DataTypes.DECIMAL),
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
  },
  status: {
    type: DataTypes.ENUM('pending', 'completed'),
    defaultValue: 'pending',
  },
  assignedTo: {
    type: DataTypes.UUID,
    allowNull: true,
  },
  assignedAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
});
export default Task;
