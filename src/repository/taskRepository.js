import Task from '../models/tasks.js';

/**
 * Create a new task.
 *
 * @param {Object} taskData - The task data.
 * @param {Array<number>} taskData.location - The location coordinates as [latitude, longitude].
 * @param {string} [taskData.description] - The task description.
 * @param {('pending'|'completed')} [taskData.status] - The task status.
 * @param {string|null} [taskData.assignedTo] - The user assigned to the task.
 * @param {Date|null} [taskData.assignedAt] - The date the task was assigned.
 * @returns {Promise<Object>} A Promise that resolves to the created task.
 * @throws {Error} If the task data is invalid.
 */
export async function saveTask(taskData) {
  return Task.create(taskData);
}

/**
 * Get a task by its ID.
 *
 * @param {string} taskId - The ID of the task to retrieve.
 * @returns {Promise<Object>} A Promise that resolves to the retrieved task.
 * @throws {Error} If the taskId is not a valid number or if the task is not found.
 */
export async function findTaskById(taskId) {
  const task = await Task.findOne({
    where: {
      taskId,
    },
  });
  if (!task) {
    throw new Error('Task not found');
  }
  return task;
}


/**
 * Update a task by its ID.
 *
 * @param {string} taskId - The ID of the task to update.
 * @param {Object} taskData - The updated task data.
 * @returns {Promise<Object>} A Promise that resolves to the updated task.
 * @throws {Error} If the taskId is not a valid number, if the task data is invalid, or if the task is not found.
 */
export async function updateTask(taskId, taskData) {
  const task = await findTaskById(taskId);
  // Update task properties with taskData
  Object.assign(task, taskData);
  await task.save();
  return task;
}

/**
 * Find and count all tasks with pagination.
 *
 * @param {number} offset - The offset for pagination.
 * @param {number} limit - The limit for the number of tasks to retrieve.
 * @returns {Promise<{rows: Array<Task>, count: number}>} - An object containing an array of task rows and the total count of tasks.
 */
export async function findAll(offset, limit) {
  return Task.findAndCountAll({
    offset,
    limit,
  });
}
