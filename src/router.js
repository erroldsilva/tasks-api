import {
  assignTaskHandler,
  createTaskHandler,
  getTaskByIdHandler,
  listTasksHandler,
} from './controllers/taskController.js';
import {
  assignTaskSchema,
  createTaskSchema,
  getAllTasksSchema,
  getTaskByIdSchema,
} from './controllers/taskSchemas.js';

async function taskRouter(fastify, opts, done) {
  // Create a new task
  fastify.post('/api/tasks', { schema: createTaskSchema }, createTaskHandler);
  // Get a task by ID
  fastify.get('/api/tasks/:taskId', { schema: getTaskByIdSchema }, getTaskByIdHandler);
  // List all tasks
  fastify.get('/api/tasks', { schema: getAllTasksSchema }, listTasksHandler);
  // Assign a task to a specific worker
  fastify.put('/api/tasks/:taskId/assign', { schema: assignTaskSchema }, assignTaskHandler);

  done();
}

export default taskRouter;
