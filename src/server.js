import server from './app.js';

const PORT = process.env.PORT || 9000;

const start = async () => {
  try {
    await server.listen({ port: PORT });
    console.log(`Server is running on port ${PORT}`);

  } catch (err) {
    console.error('Error starting server:', err);
    process.exit(1);
  }
};

start();
