import { findAll, findTaskById, saveTask, updateTask } from '../repository/taskRepository.js';
import newTaskUseCase from './useCases/newTaskUseCase.js';
import assignTaskUseCase from './useCases/assignTaskUseCase.js';

/**
 * Create a new task with a 'pending' status.
 *
 * @param {Object} taskData - The task data to create.
 * @returns {Promise<Task>} - A promise that resolves to the created task.
 */
export async function createTask(taskData) {
  const taskDataDto = newTaskUseCase(taskData);
  return saveTask(taskDataDto);
}

/**
 * Get a task by its ID.
 *
 * @param {string} taskId - The ID of the task to retrieve.
 * @returns {Promise<Task|null>} - A promise that resolves to the task if found, or null if not found.
 */
export async function getTaskById(taskId) {
  return findTaskById(taskId);
}

/**
 * Assign a task to a worker if it's not already assigned.
 *
 * @param {string} taskId - The ID of the task to assign.
 * @param {string} assignedTo - The worker to assign the task to.
 * @returns {Promise<Task>} - A promise that resolves to the assigned task.
 * @throws {Error} - Throws an error if the task is already assigned.
 */
export async function addAssignee(taskId, assignedTo) {
  const task = await findTaskById(taskId);
  const updatedTask = assignTaskUseCase(task, assignedTo);
  return updateTask(taskId, updatedTask);

}


/**
 * Get a paginated list of tasks.
 *
 * @param {number} page - The page number to retrieve (default is 1).
 * @param {number} pageSize - The number of tasks to retrieve per page (default is 10).
 * @returns {Promise<{tasks: Array<Task>, totalTasks: number, currentPage: number, totalPages: number}>} - Paginated tasks and metadata.
 */
export async function listTasks(page = 1, pageSize = 10) {
  const offset = (page - 1) * pageSize;

  const { rows, count } = await findAll(offset, pageSize);

  const totalPages = Math.ceil(count / pageSize);

  return {
    tasks: rows,
    totalTasks: count,
    currentPage: page,
    totalPages: totalPages,
  };
}

