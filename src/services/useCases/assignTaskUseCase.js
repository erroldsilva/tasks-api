/**
 * Assigns a task to a worker if the task is pending and unassigned.
 *
 * @param {object} task - The task to be assigned.
 * @param {string} assignedTo - The worker to assign the task to.
 * @returns {object} The updated task with assigned worker and timestamp.
 * @throws {Error} If the task is already assigned or not in pending status.
 */
export default function assignTaskUseCase(task, assignedTo) {
  if (task.assignedTo === null && task.status === 'pending') {
    return {
      assignedTo,
      assignedAt: new Date(),
    };
  } else {
    throw new Error('Task is already assigned or not in pending status');
  }
}
