import assignTaskUseCase from './assignTaskUseCase.js';

describe('assignTaskUseCase', () => {
  it('should assign a task if it is pending and unassigned', () => {
    const task = {
      assignedTo: null,
      status: 'pending',
    };
    const assignedTo = 10;
    const result = assignTaskUseCase(task, assignedTo);
    expect(result.assignedTo).toBe(assignedTo);
    expect(result.assignedAt).toBeDefined();
  });

  it('should throw an error if the task is already assigned', () => {
    const task = {
      assignedTo: 10,
      status: 'pending',
    };
    const assignedTo = 'Bob';
    expect(() => assignTaskUseCase(task, assignedTo))
      .toThrowError('Task is already assigned or not in pending status');
  });

  it('should throw an error if the task status is not pending', () => {
    const task = {
      assignedTo: null,
      status: 'completed',
    };
    const assignedTo = 'John Doe';
    expect(() => assignTaskUseCase(task, assignedTo))
      .toThrowError('Task is already assigned or not in pending status');
  });
});
