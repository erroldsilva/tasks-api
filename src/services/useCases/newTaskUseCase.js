/**
 * Creates a new task with the provided data.
 *
 * @param {object} taskData - The data for creating the task.
 * @param {number[]} taskData.location - An array containing the latitude and longitude of the task location.
 * @param {string} taskData.description - The description of the task.
 * @returns {object} The newly created task with updated status.
 * @throws {Error} If the task location does not have exactly 2 elements.
 */
export default function newTaskUseCase(taskData) {
  if (taskData.location.length !== 2) {
    throw Error('task location is not right');
  }
  taskData.status = 'pending';
  return taskData;
}
