import newTaskUseCase from './newTaskUseCase.js';

describe('newTaskUseCase', () => {
  it('should return a task with status pending if location has 2 coordinates', () => {
    const taskData = {
      location: [1, 2],
    };
    const result = newTaskUseCase(taskData);
    expect(result.status).toBe('pending');
  });

  it('should throw an error if location does not have 2 coordinates', () => {
    const taskData = {
      location: [1, 2, 3], // More than 2 coordinates
    };
    expect(() => newTaskUseCase(taskData)).toThrowError('task location is not right');
  });
});
